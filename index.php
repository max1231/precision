<?php
$array_content = array(
  array(),
);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="styles.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Dogs</title>
  </head>
  <body>
    <section id="header" class="top-header">
        <header class="clearfix">
        <div class="logo">
          <a href="#/">
            <img src="https://cdn.optipic.io/site-100590/media/logo/default/logo_nuevo_1.png"/>
          </a>
        </div>
        <div id="barra-header" class="container">
          <p style="font-size:14px; margin-top: 5px">Prueba Técnica Precision</p>
        </div>
      </header>
    </section>  

  <div class = "container">

    <?php
      $url = "https://dog.ceo/api/breeds/list/all";
      $json = file_get_contents($url); 
      $data = json_decode($json,true);
      $dogs = $data->message[0];
      //echo('<pre>');
      //var_dump($data);
      //print_r($dogs);
      //echo('</pre>');
      // foreach($data->data as $dog => $subdog) 
      //   {
      //     if (is_array($subdog) || is_object($subdog))
      //     {
      //       foreach ($subdog->breed as $breed) 
      //       {
      //         echo $breed->dogs . "\n";
      //       }
      //     }  
      //   }
      
    ?>
    <script>
      fetch('https://dog.ceo/api/breeds/list/all')
        .then(response => response.json())
        .then(json => console.log(json))
    </script>

    <div class="panel-group" id="accordion">
      <?php $i=1; foreach ($array_content as $acontent) { ?>    
        <?php foreach(range('a','z') as $letter) {?> 
          <div class="panel panel-default"> 
            <div class="panel-heading"> 
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>">
                  <?php echo strtoupper($letter); ?>
                </a>
              </h4>
            </div>
          <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse">
      <div class="panel-body">
        <!-- Aqui va la raza de perro segun con la letra que empieza-->  
    </div>
  </div> 
</div>       
  <?php $i++; } ?>
    </div>    
    <?php } ?>  
  </div>
</div> <!-- end container -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    
  </body>

<footer id="footer" class="container-fluid text-center bg-lightgray" style="background-color: #20272f; color: #fff;">
  <div class="copyrights" style="margin-top:25px;">
      <p>Precision © 2021
          <br>
          <span>Diseñado por: Max K&ouml;hler</span></p>
      <p><a href="https://www.linkedin.com/in/mkohlers/" target="_blank">Linkedin <i class="fa fa-linkedin-square" aria-hidden="true"></i> </a></p>
  </div>
</footer>
</html>